import json
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
from models.models import TwoLayerNet
import os
import matplotlib.pyplot as plt

# device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# config & train data
filename = 'train_data/[Minute][NextFT]_to_[NextFH].json'
with open(filename) as json_file:  
    data = json.load(json_file)

input_dimensions = 4
output_dimensions = 3
batch_size = 1
# config & train data

# Inputs
inputs = np.array(data["input"], dtype='float32')
targets = np.array(data["output"], dtype='float32')
inputs = torch.from_numpy(inputs).to(device)
targets = torch.from_numpy(targets).to(device)
train_ds = TensorDataset(inputs, targets)
train_dl = DataLoader(train_ds, batch_size, shuffle=True)

# Model
model = nn.Linear(input_dimensions, output_dimensions)
# Define optimizer
opt = torch.optim.SGD(model.parameters(), lr=1e-5)
# Define loss function
loss_fn = F.mse_loss

# pre train func
epoch_num = 0
mse_list = []
epoch_list = []

# model save
SAVE_PATH = os.path.dirname(os.path.realpath(__file__))+'/save/twoLayerNet/nextfh_linear_1.pth'
# model load
checkpoint = torch.load(SAVE_PATH)
model.load_state_dict(checkpoint['model_state_dict'])
opt.load_state_dict(checkpoint['optimizer_state_dict'])
model.eval()
epoch_num = checkpoint['epoch']
loss = checkpoint['loss']

def info():
    print(data["dictionary"])
    #print("Trainset :", data["info"]["count_in/out"])
    print("device : ", device)
    print("input_dimensions : ", input_dimensions)
    print("output_dimensions : ", output_dimensions)
    print("batch_size : ", batch_size)
    print("optimizer : ", opt)
    print("epoch : ", epoch_num)
    print("loss : ", loss)
    print(model)
    # print

def fit(train_for, epoch_num, model, loss_fn, opt):
    for epoch in range(train_for):
        epoch_num += 1
        for xb,yb in train_dl:
            # Generate predictions
            pred = model(xb)
            loss = loss_fn(pred, yb)
            # Perform gradient descent
            loss.backward()
            opt.step()
            opt.zero_grad()
            # debug data
            epoch_list.append(epoch_num)
            mse_list.append(loss_fn(model(inputs), targets).item())
            print('Training loss: ',epoch, loss_fn(model(inputs), targets))            
    return epoch_num

def menu(command, epoch_num, model, loss_fn, opt):
    if(command == "info"):
        info()
    elif(command == "train"):
        epoch_num = fit(5000, epoch_num, model, loss_fn, opt)

        plt.plot(epoch_list, mse_list)
        plt.ylabel('mse_loss')
        plt.xlabel('epoch')
        plt.show()

        torch.save({
                    'epoch': epoch_num,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': opt.state_dict(),
                    'loss': loss_fn(model(inputs), targets).item(),
                    'iho' : [input_dimensions, output_dimensions],
                    'train_file' : filename
                    }, SAVE_PATH)
        return False          
    elif():
        print("Wrong command")


while True:
    print("Menu - type")
    print(">>> info - for current model details")
    print(">>> train - for additional training of model")
    print(">>> crtl + c - to exit")
    command = input(">>> ")
    res = menu(command, epoch_num, model, loss_fn, opt)
    if(res==False):
        break
