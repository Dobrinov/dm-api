# python app.py
# Running on http://127.0.0.1:5000/

# import falsk
from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

# recourse list
users = [
    {
        "name": "Nicholas",
        "age": 42,
        "occupation": "Network Engeneer"
    },
    {
        "name": "Andrea",
        "age": 56,
        "occupation": "Doctor"
    },
    {
        "name": "Maria",
        "age": 22,
        "occupation": "Student"
    }        
]

# model
class User(Resource):

    def get(self, name):
        for user in users:
            if(name == user["name"]):
                return user, 200
        return "User not found", 404        

    def post(self, name):
        parser = reqparse.RequestParser()
        parser.add_argument("age")
        parser.add_argument("occupation")
        args = parser.parse_args()

        for user in users:
            if(name == user["name"]):
                return "User with name {} already exists".format(name), 404

        user = {
            "name": name,
            "age": args["age"],
            "occupation": args["occupation"]
        }
        users.append(user)
        return user, 201        

    def put(self, name):
        parser = reqparse.RequestParser()
        parser.add_argument("age")
        parser.add_argument("occupation")
        args = parser.parse_args()

        for user in users:
            if(name == user["name"]):
                user["age"] = args["age"]
                user["occupation"] = args["occupation"]
                return user, 200

        user = {
            "name": name,
            "age": args["age"],
            "occupation": args["occupation"]
        }
        users.append(user)
        return user, 201

    def delete(self, name):
        global users
        users = [user for user in users if user["name"] != name]
        return "{} is delted.".format(name), 200

class UserList(Resource):
    def get(self):
        return users

api.add_resource(UserList, "/user")
api.add_resource(User, "/user/<string:name>")
app.run(debug=True)        